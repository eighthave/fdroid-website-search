from django import template
from fdroid_website_search import default_config

from fdroid_website_search.models import FDroidSite

import fdroid_website_search.settings


register = template.Library()

@register.assignment_tag
def get_all_repos():
    return default_config.fdroid


@register.assignment_tag
def get_color_background():
    return default_config.color_background


@register.assignment_tag
def get_color_background_header():
    return default_config.color_background_header


@register.assignment_tag
def get_color_text():
    return default_config.color_text


@register.assignment_tag
def get_link_back():
    return default_config.fdroid[0]['site']


@register.assignment_tag
def get_last_updated():
    return '{:%Y-%d-%m}'.format(FDroidSite.objects.first().lastUpdated)


@register.assignment_tag
def get_version():
    return fdroid_website_search.settings.VERSION
