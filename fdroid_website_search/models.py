#!/usr/bin/env python3
#
# models.py - part of fdroid-website-search django application
# Copyright (C) 2017 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models


import datetime


class Language(models.Model):
    name = models.CharField('name', max_length=255)
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class FDroidSite(models.Model):
    repoUrl = models.CharField('repository URL', max_length=255)
    siteUrl = models.CharField('website URL', max_length=255)
    lastUpdated = models.DateField('last updated', default=datetime.date.today)
    def __str__(self):
        return str(self.siteUrl).replace('http://', '').replace('https://', '')


class Category(models.Model):
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class AntiFeature(models.Model):
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class License(models.Model):
    name = models.CharField('name', max_length=255) 
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('name',)


class App(models.Model):

    name = models.CharField('name', max_length=255)
    packageName = models.CharField('package name', max_length=255)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField()
    whatsNew = models.TextField()

    authorName = models.CharField('authorName', max_length=255)
    authorEmail = models.CharField('authorEmail', max_length=255)

    categories = models.ManyToManyField(Category)
    antiFeatures = models.ManyToManyField(AntiFeature)
    license = models.ForeignKey(License, null=True)

    webSite = models.CharField('webSite', max_length=255)
    sourceCode = models.CharField('sourceCode', max_length=255)
    issueTracker = models.CharField('issueTracker', max_length=255)
    changelog = models.CharField('changelog', max_length=255)

    donate = models.CharField('donate', max_length=255)
    bitcoin = models.CharField('bitcoin', max_length=255)
    litecoin = models.CharField('litecoin', max_length=255)
    flattrID = models.CharField('flattrID', max_length=255)

    added = models.DateTimeField()
    lastUpdated = models.DateTimeField()

    icon = models.CharField('icon', max_length=255)
    site = models.ForeignKey(FDroidSite, verbose_name="F-Droid website")
    def i18n_data(self):
        i18n = AppI18n.objects.filter(entry=self)
        return [i for i in i18n if i.lang.name.startswith('en')]
    def i18n_summary(self):
        i18n = self.i18n_data()
        if len(i18n) == 0 or len(i18n[0].summary) == 0:
            return self.summary
        return i18n[0].summary
    def siteUrl(self):
        return self.site.siteUrl + '/packages/' + self.packageName
    def iconUrl(self):
        return self.site.repoUrl + '/icons/' + self.icon
    def __str__(self):
        return self.name
    class Meta():
        unique_together = ('packageName', 'site')

class AppI18n(models.Model):

    name = models.CharField('name', max_length=255, blank=True)
    summary = models.CharField('summary', max_length=255, blank=True)
    description = models.TextField(blank=True)

    whatsNew = models.TextField(blank=True)

    lang = models.ForeignKey(Language, verbose_name="language")
    entry = models.ForeignKey(App, verbose_name="package")
    class Meta():
        unique_together = ('lang', 'entry')
